import {invalidSchema} from './../data/invalidSchema';
import Ajv from 'ajv';
import chai, {expect, assert} from 'chai';

describe('Invalid Schema', () => {
    let ajv;

    before(() => ajv = new Ajv({allErrors: true}));

    it('should return false when schema is invalid', () =>
        assert.isFalse(ajv.validateSchema(invalidSchema))
    );

    it('should throw error when invalid schema is compiled', () =>
        chai.should().throw(() => ajv.compile(invalidSchema))
    );
});