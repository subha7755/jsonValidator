import {schemaWithCustomAndDefaultValidatedFields} from './../data/schemaWithCustomAndDefaultValidatedFields';
import Ajv from 'ajv';
import {assert} from 'chai';

describe('Custom and Default field validation', () => {
    let ajv;

    before(() =>{
        ajv = new Ajv({allErrors: true});
        ajv.addKeyword('validateProduct',
            {validate: (schema, data) => {
                return schema.departments.indexOf(data) > 0
            },
            metaSchema: {
                type: 'object',
                properties: {
                    departments: { type: 'array' }
                },
                additionalProperties: true
            }})
    });

    it('should return true when data matches schema with custom type', () => {
        const data = {department: 'Wine', foo: 'dummy'};
        let compiledSchema = ajv.compile(schemaWithCustomAndDefaultValidatedFields);
        assert.isTrue(compiledSchema(data));
    });

    it('should return false when data does not match custom validation', () => {
        const data = {department: 'Baby', foo: 'dummy'};
        let compiledSchema = ajv.compile(schemaWithCustomAndDefaultValidatedFields);
        assert.isFalse(compiledSchema(data));
    });

    it('should return false when data does not match default validation', () => {
        const data = {department: 'Wine', foo: 4};
        let compiledSchema = ajv.compile(schemaWithCustomAndDefaultValidatedFields);
        assert.isFalse(compiledSchema(data));
    });
});