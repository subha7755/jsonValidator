import {schemaWithSimpleTypeValidation} from './../data/schemaWithSimpleTypes';
import Ajv from 'ajv';
import {assert} from 'chai';

describe('Simple type validation', () => {
    let ajv;

    before(() => ajv = new Ajv({allErrors: true}));

    it('should return true when data type matches schema type', () => {
        const data = {foo: "hello", bar: 3, foobar: {one: {onePointOne: [1, 2], onePointTwo: 'sdf'}}};
        assert.isTrue(ajv.compile(schemaWithSimpleTypeValidation)(data));
    });

    it('should return false when data does not match schema constraints like maxValue', () => {
        const data = {foo: "hello", bar: 4, foobar: {one: {onePointOne: [1, 2], onePointTwo: 'sdf'}}};
        let compiledSchema = ajv.compile(schemaWithSimpleTypeValidation);
        assert.isFalse(compiledSchema(data));
        assert.equal(ajv.errorsText(compiledSchema.errors), 'data.bar should be <= 3');
    });

    it('should return false when data does not match nested schema constraints', () => {
        const data = {foo: "hello", bar: 3, foobar: {one: {onePointOne: 3, onePointTwo: 3}}};
        let compiledSchema = ajv.compile(schemaWithSimpleTypeValidation);
        let compiledSchema2 = compiledSchema(data);
        assert.isFalse(compiledSchema2);
    });
});