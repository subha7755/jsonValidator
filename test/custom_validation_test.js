import {schemaWithCustomValidatedFields} from './../data/schemaWithCustomValidatedFields';
import Ajv from 'ajv';
import {assert} from 'chai';

describe('Custom field validation', () => {
    let ajv;

    before(() =>{
        ajv = new Ajv({allErrors: true});
        ajv.addKeyword('validateDepartment',
            {validate: (schema, data) => schema.department.indexOf(data.department) > 0})
    });

    it('should return true when data matches schema with custom type', () => {
        const data = {department: 'Wine'};
        let compiledSchema = ajv.compile(schemaWithCustomValidatedFields);
        assert.isTrue(compiledSchema(data));
    });

    it('should return false when custom field data does not match schema with custom type', () => {
        const data = {department: 'Baby'};
        assert.isFalse(ajv.compile(schemaWithCustomValidatedFields)(data));
    });
});