import {schemaWithFormats} from './../data/schemaWithFormats';
import Ajv from 'ajv';
import {assert} from 'chai';

describe('Format validation', () => {
    let ajv;

    before(() => ajv = new Ajv({allErrors: true}));

    it('should return true when data type matches schema type', () => {
        const data = {email: 'abc@example.com', orderDate:'2017-05-19'};
        assert.isTrue(ajv.compile(schemaWithFormats)(data));
    });

    it('should return false when data does not match schema type', () => {
        const data = {email: 'emailwithoutformat', orderDate:'2017-05-19'};
        assert.isFalse(ajv.compile(schemaWithFormats)(data));
    });
});