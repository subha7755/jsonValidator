import {imageSchema} from './../data/imageSchema';
import Ajv from 'ajv';
import {assert} from 'chai';

describe('Image schema validation', () => {
    let ajv;

    before(() => ajv = new Ajv({allErrors: true}));

    it('should return true when data type matches schema type', () => {
        const data = {
            "schema": {
                "name": "image",
                "version": "1.0.0"
            },
            "data": {
                "images": [
                    {
                        "width": 300,
                        "height": 200,
                        "name": "eSpot2_300x200",
                        "ext": ".png",
                        "url": "/content/v2/assets/images/kroger-rewards-visa-mobile/eSpot2_300x200.png"
                    }
                ]
            }
        };
        assert.isTrue(ajv.compile(imageSchema)(data));
    });

    it('should return false when required fields are missing', () => {
        const data = {
            "schema": {
                "version": "1.0.0"
            },
            "data": {
                "images": [
                    {
                        "width": 300,
                        "height": 200,
                        "name": "eSpot2_300x200",
                        "ext": ".png",
                        "url": "/content/v2/assets/images/kroger-rewards-visa-mobile/eSpot2_300x200.png"
                    }
                ]
            }
        };
        assert.isFalse(ajv.compile(imageSchema)(data));
    });
});