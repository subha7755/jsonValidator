export const schemaWithFormats = {
    properties: {
        email: {type: 'string', format: 'email'},
        orderDate: {type:'string', format: 'date'}
    }
};