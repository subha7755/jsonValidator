export const invalidSchema = {
    "properties": {
        "foo": { "type": "invalidtype" },
        "bar": { "type": "number", "maximum": 3 },
    }
};