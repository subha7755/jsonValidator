export const schemaWithSimpleTypeValidation = {
    "properties": {
        "foo": { "type": "string" },
        "bar": { "type": "number", "maximum": 3 },
        "foobar":{
            type: 'object',
            properties: {
                one: {
                    type: 'object',
                    properties: {
                        onePointOne: {type: 'array'},
                        onePointTwo: {type: 'string'}
                    }
                }
            }
        }
    }
};