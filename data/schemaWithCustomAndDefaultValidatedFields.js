export const schemaWithCustomAndDefaultValidatedFields = {
    properties: {
        foo: {type: 'string'},
        department: {
            type: 'string',
            validateProduct: {
                departments: ['Grocery', 'Wine']
            }
        }
    },
    additionalProperties: false
};