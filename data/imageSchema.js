export const imageSchema = {
    id: 'imageSchema.js#',
    "properties": {
       schema: {
           type: 'object',
           required: ['name', 'version'],
           properties: {
               name: {type: 'string'},
               version: {type: 'string'}
           }
       },
        data:{
           type: 'object',
            required: ['images'],
            properties: {
               images: {
                   type: 'array',
                   items: { "$ref": "imageSchema.js#" }
               }
            }

        }
    }
};
